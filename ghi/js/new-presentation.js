window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('conference');
        console.log(data);
        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href
            option.innerHTML = conference.name;
            selectTag.appendChild(option)
        }
    }
    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const selectTag = document.getElementById('conference');
        const selectedOption = selectTag.options[selectTag.selectedIndex];
        const presentationUrl = `http://localhost:8000${selectedOption.value}presentations/`;
        console.log(presentationUrl)
        console.log(selectTag.selectedIndex)
        console.log(selectedOption)
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
        }
    });
});
