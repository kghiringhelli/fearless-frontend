import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [presentationMax, setPresentationMax] = useState('');
    const [attendeeMax, setAttendeeMax] = useState('');
    const [location, setLocation] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        console.log(response)
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = presentationMax;
        data.max_attendees = attendeeMax;
        data.location = location;
        console.log(data);
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
        setName('');
        setStart('');
        setEnd('');
        setDescription('');
        setPresentationMax('');
        setAttendeeMax('');
        setLocation('');
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handlePresentationMaxChange = (event) => {
        const value = event.target.value;
        setPresentationMax(value);
    }

    const handleAttendeeMaxChange = (event) => {
        const value = event.target.value;
        setAttendeeMax(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleStartChange} value={start} placeholder="Start date" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleEndChange} value={end} placeholder="End date" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} placeholder="" name="description" id="description" className="form-control"></textarea>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handlePresentationMaxChange} value={presentationMax} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleAttendeeMaxChange} value={attendeeMax} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    );
                })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    )
}

export default ConferenceForm;
